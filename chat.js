const amqp = require('amqplib/callback_api');
const readline = require('readline');

const uri = "amqp://saul:123456@localhost:32781";
//const uriAWS = "amqp://saul:abcde1234564874@amqps://b-f11a37fd-7b0d-4353-8d63-dbba9beb2d3d.mq.us-east-1.amazonaws.com:5671"

function crearConexion(nombreCola) {
  amqp.connect(uri, (err, con) => {
    if (err) {
      throw err;
    }
    con.createChannel((errCh, channel) => {
      if (errCh) {
        throw errCh;
      }

      channel.assertQueue(nombreCola, {
        durable: false
      });

      console.log(`Conexión creada para la cola "${nombreCola}"`);

      channel.consume(nombreCola, (msg) => {
        console.log(`[${nombreCola}] Mensaje recibido: ${msg.content.toString()}`);
      }, { noAck: true });

      // Leer mensajes desde la consola y enviarlos a la cola
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      });

      rl.on('line', (input) => {
        channel.sendToQueue(nombreCola, Buffer.from(input));
      });
    });
  });
}

const participante1 = 'Saul';
const participante2 = 'Persona 2';

const nombreCola = `${participante1}-${participante2}`;

crearConexion(nombreCola);
