# Sala de Chat

Utilizando el conocimiento adquirido en clase vamos a crear una sala de chat. el chat room es de solo 2 participantes con la opción de crear varias conexiones donde el nombre de la cola será el nombre de las dos personas que estén hablando entre si, como entregable se espera el código fuente del proyecto en git y en el archivo readme.md las instrucciones para hacerlo funcionar.


## Instalación para su funcionamiento
1. Clona el repositorio: `https://gitlab.com/Poncianiix/chat-rabbit.git`
2. Si quieres usar el servicio de AWS crea tu servidor de rabbit y modifica el uri o utiliza docker y tambien 
modifica el uri.

```javascript
// Ejemplo del uri
const uri = "amqp://saul:123456@localhost:32781";
const uriAWS = "amqp://saul:abcde1234564874@amqps://b-f11a37fd-7b0d-4353-8d63-dbba9beb2d3d.mq.us-east-1.amazonaws.com:5671"
```
3. Con el punto anteiror aclarado navega al directorio del proyecto: `cd chat-rabbit`
4. Instala las dependencias: `npm install`
5. Ejecuta: `node chat.js`
6. Repite los pasos anteriores para el otro usuario del chat.

## Uso
1. Al ejecutar el comando `node chat.js` y que los dos usuarios esten conectados el chat empezara a funcionar.
2. Ya se pueden enviar mensajes los dos usuarios.